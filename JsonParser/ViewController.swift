//
//  ViewController.swift
//  JsonParser
//
//  Created by Anand Yadav on 05/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var userData = [User]()
    var commentsData = [Comments]()
    var posts = [Posts]()
    var albumsData = [Albums]()
    //var photos = [Photos]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        segmentControl.selectedSegmentIndex = 0
        
        ServiceManager.getPosts(success: { (response) in
            let posts = response as! [Posts]
            print("**********POSTS**********")
            for post in posts {
                print(post.id!)
            }
        }) { (error) in
            debugPrint(error as Any)
        }
    }
    @IBAction func segmentChanged(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            ServiceManager.getAlbums(success: { (response) in
                self.albumsData = response as! [Albums]
                self.tableView.reloadData()
                print("**********ALBUMS**********")
            }) { (error) in
                debugPrint(error as Any)
            }
        case 1:
            ServiceManager.getUsers(success: { (response) in
                self.userData = response as! [User]
                self.tableView.reloadData()
                print("**********USER**********")
            }) { (error) in
                debugPrint(error as Any)
            }
        case 2:
            ServiceManager.getComments(success: { (response) in
                self.commentsData = response as! [Comments]
                self.tableView.reloadData()
                print("**********USER**********")
            }) { (error) in
                debugPrint(error as Any)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.segmentControl.selectedSegmentIndex {
        case 0:
            return albumsData.count
        case 1:
            return userData.count
        case 2:
            return commentsData.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.segmentControl.selectedSegmentIndex {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell") as! AlbumCell
            cell.id.text = "\(albumsData[indexPath.row].id!)"
            cell.userId.text = "\(albumsData[indexPath.row].userId!)"
            cell.title.text = albumsData[indexPath.row].title
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as! UserCell
            cell.name.text = userData[indexPath.row].name
            cell.username.text = userData[indexPath.row].username
            cell.email.text = userData[indexPath.row].email
            cell.city.text = userData[indexPath.row].address?.city
            cell.company.text = userData[indexPath.row].company?.name
            cell.street.text = userData[indexPath.row].address?.street
            cell.suite.text = userData[indexPath.row].address?.suite
            cell.phone.text = userData[indexPath.row].phone
            cell.zip.text = userData[indexPath.row].address?.zipcode
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as! CommentCell
            cell.id.text = "\(commentsData[indexPath.row].id!)"
            cell.name.text = "\(commentsData[indexPath.row].name!)"
            cell.email.text = "\(commentsData[indexPath.row].email!)"
            cell.body.text = "\(commentsData[indexPath.row].body!)"
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
}

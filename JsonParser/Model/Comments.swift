//
//  Comments.swift
//  JsonParser
//
//  Created by Anand Yadav on 07/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct Comments: Codable {
    let postID, id: Int?
    let name, email, body: String?

    enum CodingKeys: String, CodingKey {
        case postID
        case id, name, email, body
    }
}
//{
//  "postId": 1,
//  "id": 1,
//  "name": "id labore ex et quam laborum",
//  "email": "Eliseo@gardner.biz",
//  "body": "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
//}

//
//  Posts.swift
//  JsonParser
//
//  Created by Anand Yadav on 08/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct Posts: Codable {
    let userID, id: Int?
    let title, body: String?

    enum CodingKeys: String, CodingKey {
        case userID
        case id, title, body
    }
}

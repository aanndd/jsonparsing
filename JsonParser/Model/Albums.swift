//
//  Albums.swift
//  JsonParser
//
//  Created by Anand Yadav on 07/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct Albums: Codable {
    let userId, id: Int?
    let title: String?

    enum CodingKeys: String, CodingKey {
        case userId
        case id, title
    }
}

//
//  CommentCell.swift
//  JsonParser
//
//  Created by Anand Yadav on 08/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var body: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

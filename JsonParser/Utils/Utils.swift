//
//  Utils.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class Utils: NSObject {
    func parseJsonData(jsonData: Data) -> [User] {
        var user = [User]()
        let decoder = JSONDecoder()
        do {
            user = try decoder.decode(Array<User>.self, from: jsonData)
        } catch {
            print(error)
        }
     
        return user
    }
}
extension Decodable {
    static func map(JSONData:Data) -> Self? {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(Self.self, from: JSONData)
        } catch let error {
            print(error)
            return nil
        }
    }
}

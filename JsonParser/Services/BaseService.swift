//
//  BaseService.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

class BaseService: NSObject {
    
    func apiRequest(url:URLRequestConvertible, success: @escaping (_ response: Any?)-> Void, failure: @escaping (_ error:Error?)-> Void) {
        
        Alamofire.request(url).validate().responseData { response in
            switch response.result {
            case .success(let data):
                print("Validation Successful")
                debugPrint(data)
                success(data)
                
            case .failure(let error):
                print(error)
                failure(response.result.error)
            }
        }
    }
}

//
//  ServiceManager.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    
    static func getUsers(success: @escaping (_ response: Any?) -> Void,
                         failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getUsers, success: { (response) in
            //let userData = Utils().parseJsonData(jsonData: response as! Data)
            let userData = [User].map(JSONData:response as! Data)!
            success(userData)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getComments(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getComments, success: { (response) in
            let comments = [Comments].map(JSONData:response as! Data)!
            success(comments)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getAlbums(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getAlbum, success: { (response) in
            let comments = [Albums].map(JSONData:response as! Data)!
            success(comments)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getPosts(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getPosts, success: { (response) in
            let posts = [Posts].map(JSONData:response as! Data)!
            success(posts)
        }) { (error) in
            failure(error)
        }
    }
}

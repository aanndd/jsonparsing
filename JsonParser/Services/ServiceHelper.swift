//
//  ServiceHelper.swift
//  JsonParser
//
//  Created by Anand Yadav on 06/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

enum ServiceHelper: URLRequestConvertible
{
    case getAlbum
    case getUsers
    case getPosts
    case getComments
    case createUser(parameters: Parameters)
    case readUser(username: String)
    case updateUser(username: String, parameters: Parameters)
    case destroyUser(username: String)
    
    static let baseURLString = "https://jsonplaceholder.typicode.com"
//    JSONPlaceholder comes with a set of 6 common resources:
//    https://app.quicktype.io/         Creare model class
//    /posts    100 posts
//    /comments    500 comments
//    /albums    100 albums
//    /photos    5000 photos
//    /todos    200 todos
//    /users    10 users
    
    var method: HTTPMethod {
        switch self {
        case .getAlbum:
            return .get
        case .getComments:
            return .get
        case .getPosts:
            return .get
        case .getUsers:
            return .get
        case .createUser:
            return .post
        case .readUser:
            return .get
        case .updateUser:
            return .put
        case .destroyUser:
            return .delete
        }
    }

    var path: String {
        switch self {
        case .getAlbum:
            return "/albums"
        case .getComments:
            return "/comments"
        case .getUsers:
            return "/users"
        case .getPosts:
            return "/posts"
        case .createUser:
            return "/users"
        case .readUser(let username):
            return "/users/\(username)"
        case .updateUser(let username, _):
            return "/users/\(username)"
        case .destroyUser(let username):
            return "/users/\(username)"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try ServiceHelper.baseURLString.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        switch self {
        case .createUser(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .updateUser(_, let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }

        return urlRequest
    }
}
